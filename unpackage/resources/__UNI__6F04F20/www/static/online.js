

        var mp3 = new Audio('/static/1154.mp3');  // 创音频对象
        var qiArr = Array.from(new Array(18), () => new Array(18))//创建二维数组
        var tempArr = [0, 0, 0];//记录棋子的位置
        var td = document.createElement('td')
        var tr = document.querySelector('tr')
        var tablepan = document.querySelector('.pan')
        var show = document.querySelector('.show')
        var userName = document.querySelector('#userName');
        var table = document.querySelector('.gezi')
        var instru = document.querySelector('#instru');
        var homeSize = 0;//房间人数
        var status = 1;//状态码，规定黑白先行规则
        var flage = 0;//阀门，设置不重复点击
	
	//    //棋子格子
	//         for (var i = 0; i < 18; i++) {
	//             var tr = document.createElement('tr')
	//             table.appendChild(tr);
	//             for (var j = 0; j < 18; j++) {
	//                 var td = document.createElement('td')
	//                 td.title = '0';
	//                 tr.appendChild(td);
	//             }
	//         }
	
	//         // 棋盘
	//         for (var i = 0; i < 17; i++) {
	//             var tr = document.createElement('tr')
	//             tablepan.appendChild(tr);
	//             for (var j = 0; j < 17; j++) {
	//                 var td = document.createElement('td')
	//                 tr.appendChild(td);
	//             }
	//         }

        var jsonData = {
            username:"",//用户名
            code: 0,//0下线；1上线；2信息
            instruction: "",//链接指令
            jstatus: 0,//0,1 状态指令，为0执黑棋先行，为1执白棋先行
            title: tempArr[2],//0,1,2 棋子信息
            temparry: tempArr//临时数组，记录棋子位置和颜色码
        }

		function test(){
			console.log("测试")
		}

        // table.addEventListener('click', judge);//事件委托
        function judge(event) {
            tr = document.querySelector('tr')
            if (event.target != table && event.target != tr) {
              if (userName.value.length == 0&&instru.value.length == 0){//创建房间才能点击
                //1黑2白
                alert("请先创建房间")
              }else if (homeSize==2){//创建房间才能点击  //1黑2白
                if (status == 0 && flage == 0) {//状态码
                  if (event.target.title == '0') {//阻止在含有棋子的位子改变样式
                    var rowIndex = event.target.parentNode.rowIndex;
                    var cellIndex = event.target.cellIndex;
                    // console.log(rowIndex, cellIndex);
                    event.target.title = '1'
                    event.target.style = "background-color:black;border-radius: 30px;border: 1px rgb(15, 15, 15) solid;"
                    qiArr[rowIndex][cellIndex] = '1';
                    //记录步骤
                    tempArr[0] = rowIndex;
                    tempArr[1] = cellIndex;
                    jsonData.title = tempArr[2] = '1';
                    jsonData.jstatus  = 1; //状态码
                    mp3.play();  // 播放
                    sendMsg(jsonData)
                    // sendMsg(jsonData)
                    // Winorlose(jsonData.temparry[0],jsonData.temparry[1],'1')
                  }
                } else if (status == 1 && flage == 0) {
                  if (event.target.title == '0') {
                    var rowIndex = event.target.parentNode.rowIndex;
                    var cellIndex = event.target.cellIndex;
                    event.target.title = '2'
                    event.target.style = "background-color:aliceblue;border-radius: 30px;border: 1px #fff solid;";
                    qiArr[rowIndex][cellIndex] = '2';
                    mp3.play();  // 播放
                    //记录步骤
                    tempArr[0] = rowIndex;
                    tempArr[1] = cellIndex;
                    jsonData.jstatus  = 0; //状态码
                    jsonData.title = tempArr[2] = '2';
                    // console.log(jsonData)
                    sendMsg(jsonData)
                    // sendMsg(jsonData)
                    // console.log(rowIndex, cellIndex);
                    // Winorlose(jsonData.temparry[0],jsonData.temparry[1],'2')
                  }
                }
              }else {
                console.log(homeSize)
                alert("房间人数不足")
              }
            }
        }
        //链接信息
        var websocket = null;
        //用来给后端发送信息
        function connect(homeLen){
          if (websocket != null) {
            return;
          }
          if (userName.value.length == 0 || instru.value.length == 0){
            alert("房间口令和用户名不能为空");
            return;
          }
          jsonData.username = userName.value;
          jsonData.instruction = instru.value;

          websocket = new WebSocket("ws://localhost:80/gobangServer/" + userName.value+"/"+instru.value+"/"+homeLen);
          homeSize++;
          //给后端发送
          // websocket.send(msg);
          //链接成功时触发
          websocket.onopen = function(){
            setMsgToPage("恭喜您，房间创建成功！");
            userName.disabled = true;
            instru.disabled = true;
          }
          //收到服务器端信息触发
          websocket.onmessage = function(event){
            var data = JSON.parse(event.data)
            if(data.code == 2 && data.instruction == jsonData.instruction){//接收到的棋子信息
              if (data.jstatus == jsonData.jstatus){//判断是不是棋手
                setMsgToPage(data.userName + "请稍后执棋子");//更新信息
                flage = 1;
              }else{
                setMsgToPage(jsonData.username + "请您执棋");//更新信息
              }
              if (data.userName != jsonData.username){
                  status = data.jstatus ;
                  flage = 0;
              }
              Rendering(data)//渲染棋盘
            }else if(data.code == 1 && data.instruction == jsonData.instruction){//上线
              setMsgToPage(data.userName+"进入房间,当前人数:"+data.homeLen);//更新信息
              homeSize = data.homeLen;
            }else if(data.code == 0 && data.instruction == jsonData.instruction){//下线
              setMsgToPage(data.userName+"下线了，恭喜你，离神之一手又近了一步");//更新信息
            }
          }
          //发生异常时触发
          websocket.onerror = function(){
            console.log("链接异常")
          }
          //链接关闭触发
          websocket.onclose = function(){
            console.log("链接关闭")
          }
        }

        //用来给后端发送信息
        function sendMsg(jsonData){
         var tempJsonDataStr =  JSON.stringify(jsonData)
          //给后端发送
          websocket.send(tempJsonDataStr);
        }

        //把信息赋值到页面
        function setMsgToPage(msg){
          show.innerHTML = msg;
        }

        //棋盘渲染函数
        function Rendering(jsonData) {
            if (jsonData.title == '1') {
                table.rows[jsonData.temparry[0]].cells[jsonData.temparry[1]].style = "background-color:black;border-radius: 30px;border: 1px rgb(15, 15, 15) solid;";
                table.rows[jsonData.temparry[0]].cells[jsonData.temparry[1]].title = jsonData.temparry[2];
                Winorlose(jsonData.temparry[0],jsonData.temparry[1],'1')
            }
            if (jsonData.title == '2') {
                table.rows[jsonData.temparry[0]].cells[jsonData.temparry[1]].style = "background-color:aliceblue;border-radius: 30px;border: 1px #fff solid;";
                table.rows[jsonData.temparry[0]].cells[jsonData.temparry[1]].title = jsonData.temparry[2];
                Winorlose(jsonData.temparry[0],jsonData.temparry[1],'2')
            }
        }

        //输赢判断
        function Winorlose(rowIndex,cellIndex,title){
          if ((updown(rowIndex, cellIndex) || leftright(rowIndex, cellIndex) || leftobli(rowIndex, cellIndex) || rightobli(rowIndex, cellIndex)) && title == '1') {
              console.log("黑棋连成5颗，黑棋获胜");
              table.removeEventListener('click', judge);
              setMsgToPage("亲，您真是太聪明了，黑棋棋手获胜！！！");//更新信息
          }
          if ((updown(rowIndex, cellIndex) || leftright(rowIndex, cellIndex) || leftobli(rowIndex, cellIndex) || rightobli(rowIndex, cellIndex)) && title == '2') {
            console.log("白棋连成5颗，白棋获胜");
            table.removeEventListener('click', judge);
            setMsgToPage("亲，您真是太聪明了，白棋棋手获胜！！！");//更新信息
          }
        }

        //保存棋盘
        function saveArr() {
            var str = document.querySelector("#instru").value
            // console.log(str.length);
            if (str.length == 0) {
                alert("输入为空,请输入指令")
            } else {
                const objToStr = JSON.stringify(qiArr)
                // console.log(objToStr);
                window.localStorage.setItem(str, objToStr)
                show.innerHTML = "客官，棋盘已保存！！！"
            }
        }

        //恢复棋盘
        function getArr() {
            var str = document.querySelector("#instru").value
            if (str.length == 0) {
                alert("输入为空,请输入指令")
            } else {
                // location.reload();
                var getValueArray = window.localStorage.getItem(str);
                qiArr = JSON.parse(getValueArray);
                for (var i = 0; i < qiArr.length; i++) {
                    for (var j = 0; j < qiArr[i].length; j++) {
                        if (qiArr[i][j] == '1') {
                            table.rows[i].cells[j].style = "background-color:black;border-radius: 30px;border: 1px rgb(15, 15, 15) solid;";
                            table.rows[i].cells[j].title = qiArr[i][j];
                        }
                        if (qiArr[i][j] == '2') {
                            table.rows[i].cells[j].style = "background-color:aliceblue;border-radius: 30px;border: 1px #fff solid;";
                            table.rows[i].cells[j].title = qiArr[i][j];
                        }
                        if (qiArr[i][j] == null) {
                            table.rows[i].cells[j].style = " width: 20px;height: 20px;cursor: pointer;border: 1px rgba(168, 23, 23, 0) solid;";
                            table.rows[i].cells[j].title = '0';
                        }
                    }
                }
                show.innerHTML = "客观，棋盘已恢复,开始下棋吧！！！"
            }
        }
        // 查看命令
        function getStruList() {
            var strCount = window.localStorage.length;
            var strList = "";
            for (var i = 0; i < strCount; i++) {
                strList += window.localStorage.key(i) + ",";
            }
          console.log(strList)
            if (strList.length > 100) {
                show.innerHTML = "亲，指令为空";
            } else {
                show.innerHTML = "亲，指令已恢复：" + strList;
            }
        }
        //移除口令
        function removeStru() {
            show.innerHTML = "亲，请输入要移除的口令";
            var str = document.querySelector("#instru").value;
            if (str.length == 0) {
                alert("口令为空！！！")
            } else {
                window.localStorage.removeItem(str);
                show.innerHTML = "亲，口令已移除！！！";
            }
        }
        //上下判断
        function updown(rowIndex, cellIndex) {
            var upNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex + i].cells[cellIndex].title == table.rows[rowIndex].cells[cellIndex].title) {
                        upNum++;
                    } else {
                        break;
                    }
                } catch (TypeError) {
                    break;
                }

            }
            var downNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex - i].cells[cellIndex].title == table.rows[rowIndex].cells[cellIndex].title) {
                        downNum++;
                    } else {
                        break;
                    }
                } catch (error) {
                    break;
                }
            }
            if (downNum + upNum + 1 < 5) {
                return false;
            } else {
                return true;
            }
        }
        //左右判断
        function leftright(rowIndex, cellIndex) {
            var leftNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex].cells[cellIndex - i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        leftNum++;
                    } else {
                        break;
                    }
                } catch (TypeError) {
                    break;
                }

            }
            var rightNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex].cells[cellIndex + i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        rightNum++;
                    } else {
                        break;
                    }
                } catch (error) {
                    break;
                }
            }
            if (leftNum + rightNum + 1 < 5) {
                return false;
            } else {
                return true;
            }
        }
        //左斜
        function leftobli(rowIndex, cellIndex) {
            var leftUpObliNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex - i].cells[cellIndex - i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        leftUpObliNum++;
                    } else {
                        break;
                    }
                } catch (TypeError) {
                    break;
                }
            }
            var rightDownNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex + i].cells[cellIndex + i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        rightDownNum++;
                    } else {
                        break;
                    }
                } catch (error) {
                    break;
                }
            }
            var sum = leftUpObliNum + rightDownNum + 1;
            // console.log(sum);
            if (leftUpObliNum + rightDownNum + 1 < 5) {
                return false;
            } else {
                return true;
            }
        }
        //右斜
        function rightobli(rowIndex, cellIndex) {
            var rightUpObliNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex - i].cells[cellIndex + i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        rightUpObliNum++;
                    } else {
                        break;
                    }
                } catch (TypeError) {
                    break;
                }
            }
            var leftDownNum = 0;
            for (var i = 1; i <= 5; i++) {
                try {
                    if (table.rows[rowIndex + i].cells[cellIndex - i].title == table.rows[rowIndex].cells[cellIndex].title) {
                        leftDownNum++;
                    } else {
                        break;
                    }
                } catch (error) {
                    break;
                }
            }
            if (rightUpObliNum + leftDownNum + 1 < 5) {
                return false;
            } else {
                return true;
            }
        }

// export { 
// 	connect,
// 	test,
// 	judge//监听事件
// }